## Service Microservice

### Appointment Model -- (used to create a service appointment)

>When creating a service appointment, the following properties are needed: VIN of vehicle, vehicle owner, date and time of appointment, reason, and assigned technician. There is also a completed property that has its default set to “False”
There is a foreign key to the Technician Model to see which technicians there are


| MODEL FIELD     | FIELD TYPE     | OTHER ARGS                                 |
| :-------------: | :------------: | :----------------------------------------- |
|      "vin"      |  `CharField`   | `max_length: 50 (required)`                |
|     "owner"     |  `CharField`   | `max_length: 100 (required)`               |
|     "date"      |  `DateField`   |                                            |
|     "time"      |  `TimeField`   |                                            |
|    "reason"     |  `TextField`   |                                            |
|  "technician"   |  `ForeignKey`  | `Technician`<br>`on_delete=models.CASCADE` |
|   "completed"   | `BooleanField` | `default = False`<br> `null=True`          |

### Technician Model -- (used to create a technician)

>When creating a technician, the following properties are needed: name and employee number


| MODEL FIELD     | FIELD TYPE     | OTHER ARGS                   |
| :-------------: | :------------: | :--------------------------- |
|     "name"      |  `CharField`   | `max_length: 100 (required)` |
|    "number"     | `IntegerField` |                              |


### AutomobileVO Model -- (used to create an Automobile)

> This model is a value object that is used for polling the inventory api. We have set up polling so that every minute or so, the inventory api sends their automobile data to service. The service microservice then takes that data and either creates or updates the automobile as an instance of the AutomobileVO.


| MODEL FIELD     |       FIELD TYPE            | OTHER ARGS                   |
| :-------------: | :-------------------------: | :--------------------------- |
|     "color"     |         `CharField`         | `max_length: 50 (required)`  |
|     "year"      | `PositiveSmallIntegerField` |                              |
|      "vin"      |         `CharField`         | `max_length: 17 (required)`  |
|  "import_href"  |         `CharField`         | `max_length: 200 (required)` |


## Sales Microservice

###  AutomobileVO Model   -- (Value object of Automobiles model in Inventory Microservice)

>This model is a value object that is used for polling the inventory api. We have set up polling so that every minute or so, the Sales Poller gets the automobile data from Inventory API. The Sales Microservice then takes that data and either creates or updates the automobile as an instance of the AutomobileVO.

|  MODEL FIELD    |    FIELD TYPE       |  OTHER ARG                   |
| :-------------: | :-----------------: | :--------------------------- |
|  "import_href"  |     `CharField`     | `max_length: 100 (required)` |
|     "color"     |     `CharField`     | `max_length: 30 (required)`  |
|     "year"      | `SmallIntegerField` | `none`                       |
|      "vin"      |     `CharField`     | `max_length: 50 (required)`  |
|     "model"     |     `CharField`     | `max_length: 50 (required)`  |
| "manufacturer"  |     `CharField`     | `max_length: 100 (required)` |
|     "sold"      |   `BooleanField`    | `default=False (required)`   |



###  SalesPerson Model   -- (used to create a Sales Person)

>When creating a Sales Person, the following properties are needed: "name" and "employee_id"


|  MODEL FIELD    |  FIELD TYPE    |  OTHER ARG                   |
| :-------------: | :------------: | :--------------------------- |
|     "name"      |  `CharField`   | `max_length: 100 (required)` |
|  "employee_id   |  `CharField`   | `max_length: 25 (required)`  |



###  Customer Model   -- (used to create a Customer)

> When creating a Customer, the following properties are needed: "name", "address", and "phone_number"

|  MODEL FIELD    |  FIELD TYPE    |  OTHER ARG                   |
| :-------------: | :------------: | :--------------------------- |
|     "name"      |  `CharField`   | `max_length: 100 (required)` |
|    "address"    |  `CharField`   | `max_length: 100 (required)` |
| "phone_number"  |  `CharField`   | `max_length: 10 (required)`  |


###  SalesRecord Model   -- (used to create a Sales Record)

> When creating a Sales Record, the following properties are needed: "automobile", "sales_person", "customer", "sales_price"

|  MODEL FIELD    |  FIELD TYPE    |  OTHER ARG                                     |
| :-------------: | :------------: | :--------------------------------------------- |
|  "automobile"   |  `ForeignKey`  | `AutomobileVO`<br>`on_delete=models.PROTECT`   |
| "sales_person"  |  `ForeignKey`  | `SalesPerson`<br>`on_delete=models.DO_NOTHING` |
|   "customer"    |  `ForeignKey`  | `Customer`<br>`on_delete=models.DO_NOTHING`    |
|  "sales_price"  |  `FloatField`  | `none`                                         |
