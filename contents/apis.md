# APIs

## Service Microservice
---
<br>

### Appointments

| Method             |                     Function                          |                     Endpoint                         |
|  ----------------  |  ---------------------------------------------------  |  --------------------------------------------------  |
|       `GET`        |                   list appointments                   |       http://localhost:8080/api/appointments/        |
|       `POST`       |                  create appointment                   |       http://localhost:8080/api/appointments/        |
|      `DELETE`      |                  delete appointment                   |     http://localhost:8080/api/appointments/:id/      |
|       `PUT`        | update appointment's <br>"completed" property to True | http://localhost:8080/api/appointments/:id/complete/ |



### Technicians


| Method             |     Function      |                Endpoint                |
|  ----------------  |  ---------------  |  ------------------------------------  |
|       `GET`        | list technicians  | http://localhost:8080/api/technicians/ |
|       `POST`       | create technician | http://localhost:8080/api/technicians

<br>

## Sales Microservice
---
<br>

### Sales Records


| Method             |     Function                |                Endpoint                |
| -----------------  |  -------------------------  |  ------------------------------------- |
|              `GET` |     list Sales Records      | http://localhost:8090/api/records/     |
|             `POST` |     create Sales Record     | http://localhost:8090/api/records/     |
|              `GET` | get details of Sales Record | http://localhost:8090/api/records/:id/ |
|           `DELETE` |     delete Sales Record     | http://localhost:8090/api/records/:id/ |

### Customers


| Method             |     Function    |                Endpoint              |
| -----------------: | :-------------: | :----------------------------------- |
|              `GET` | list Customers  | http://localhost:8090/api/customers/ |
|             `POST` | create Customer | http://localhost:8090/api/customers/ |

### Sales Persons


| Method             |     Function        |                Endpoint                 |
| -----------------: | :-----------------: | :-------------------------------------- |
|              `GET` | list Sales Persons  | http://localhost:8090/api/salespersons/ |
|             `POST` | create Sales Person | http://localhost:8090/api/salespersons/ |



### Automobile VOs


| Method             |     Function      |                Endpoint                |
| -----------------: | :-----------------: | :----------------------------------- |
|              `GET` | list Automobile VOs | http://localhost:8090/api/inventory/ |
