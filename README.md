# CarCar

## Developers
---

* Sophie Nguyen -- Service API
* John Gardner -- Sales API
<br>
<br>

## Diagram of Architecture
---

<p align="center">
	<img src="https://i.imgur.com/3UK7S6L.png"/>
</p>
<br>

## Design
---
* [API Design](contents/apis.md)
* [Data Model](contents/models.md)
* [Insomnia Inputs](contents/inputs.md)
<br><br>

## Key React Features
---

### Success message after a form submits

> After successfully submitting a form, a picture of approval will take the place of the form. I set this up by using Element.classList in the submit handler.  If the response of the submit was ok, I was able to use Element.classList to delete “d-none” from the div class that had the success picture and add “d-none” to the div that contained the form

### List of Service Appointment

> This list shows all appointments in the application. However, these appointments are being filtered to show appointments that have the “completed” property as “False”. That way, when an appointment is completed, the user can click the “completed” button, which will update the completed property to “True” for the appointment, then the appointment will not show up on this list anymore.

### VIP

> This feature checks to see if the VIN of the appointment is a VIN in Inventory.  This is where the AutomobileVO is used. If the VIN is present in Inventory, the customer is a VIP because they bought their car from the same dealership. This logic is done on the back-end using “get_extra_data” in the AppointmentListEncoder. If the VIN is present in the AutomobileVO (which is a value object that should be updating with automobiles, VIN included, from the inventory), the VIP property is “True”.

### Service Appointments History

> This feature allows the user to search from all service appointments based on a VIN. This is done with just a function and hook. I used a hook to set the input in the search bar and used that input as a filter for my list of appointments

### Sales Person History

> This feature allows the user to view all the Sales Records for a specific Sales Person. This is done with a drop down menu that loops through all of the Sales Records, finds the one that match the selected Sales Person, and changes the table to list out that Sales Person's individual sales.

### Create a Sales Record

> This feature allows the user to create a Sales Record using a Customer, Sales Person, and AutomobileVO from the database. All AutomobileVOs have a field "sold" that initiates to False. When a Sales Record is created with a specific AutomobileVO, that "sold" field updates to True. The list of AutomobileVOs that the user can choose from when creating a Sales Record is *only* the AutomobileVOs that have not been sold yet ensuring that vehicles don't get "double sold."

<br>

## Project Initialization
---

To fully enjoy this application on your local machine, please make sure to follow these steps:

1. Clone the repository down to your local machine
2. CD into the new project directory
3. Create a docker volume through your terminal and name it 'beta-data'
4. Build the docker image
5. Run the docker container
6.  Open browser to http://localhost:3000
